package com.sytrix.crane.collision;

import android.graphics.PointF;
import android.util.SizeF;

public class CollisionBox {

    CollisionManager collisionManager;
    boolean testInside;
    boolean gravity;
    float x, y;
    float w, h;
    float ix, iy;

    public CollisionBox() {
        collisionManager = null;
        testInside = true;
        gravity = false;
        x = 0.f;
        y = 0.f;
        w = 0.f;
        h = 0.f;
        ix = 0.f;
        iy = 0.f;
    }

    void setCollisionManager(CollisionManager collisionManager) {
        this.collisionManager = collisionManager;
    }

    public boolean move(float mx, float my) {
        return this.collisionManager.move(this, mx, my, 0);
    }

    public void setTestInside(boolean testInside) {
        this.testInside = testInside;
    }

    public void setGravity(boolean gravity) {
        this.gravity = gravity;
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void setSize(float w, float h) {
        this.w = w;
        this.h = h;
    }

    public boolean getTestInside() {
        return testInside;
    }

    public boolean getGravity() {
        return gravity;
    }

    public PointF getPosition() {
        return new PointF(x, y);
    }

    public SizeF getSize() {
        return new SizeF(w, h);
    }



}
