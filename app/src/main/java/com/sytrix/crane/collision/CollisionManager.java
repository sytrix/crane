package com.sytrix.crane.collision;

import java.util.ArrayList;
import java.util.List;

public class CollisionManager {

    private List<CollisionBox> listBox;
    private CollisionBox[] arrayBox;
    private float gx, gy; // gravity

    public CollisionManager() {
        listBox = new ArrayList<>();
        arrayBox = null;
        gx = 0.f;
        gy = 0.1f;
    }

    public void addBoxes(CollisionBox[] boxes) {
        for(CollisionBox box : boxes) {
            this.addBox(box);
        }
    }

    public void addBox(CollisionBox box) {
        box.setCollisionManager(this);
        listBox.add(box);

        arrayBox = new CollisionBox[listBox.size()];
        for(int i = 0; i < listBox.size(); i++) {
            arrayBox[i] = listBox.get(i);
        }
    }

    public void update() {

        int nbBox = arrayBox.length;
        for(int i = 0; i < nbBox; i++) {
            CollisionBox a = arrayBox[i];

            if(a.gravity) {
                a.ix += gx;
                a.iy += gy;

                if(!this.move(a, a.ix, a.iy, 0)) {
                    a.ix = 0.f;
                    a.iy = 0.f;
                }
            }
        }
    }

    public boolean move(CollisionBox a, float mx, float my, int fromIndex) {
        a.x += mx;
        a.y += my;

        if(this.testWithOtherBox(a, fromIndex)) {
            a.x -= mx;
            a.y -= my;
            return false;
        }

        return true;
    }

    public boolean testWithOtherBox(CollisionBox a, int fromIndex) {

        int nbBox = arrayBox.length;
        for(int j = fromIndex; j < nbBox; j++) {
            CollisionBox b = arrayBox[j];

            if(a != b) {
                if (this.testBoxBox(a, b)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean testBoxBox(CollisionBox a, CollisionBox b) {

        float lax = a.x;
        float rax = a.x + a.w;
        float lay = a.y;
        float ray = a.y + a.h;

        float lbx = b.x;
        float rbx = b.x + b.w;
        float lby = b.y;
        float rby = b.y + b.h;

        if(a.testInside) {
            if(b.testInside) {
                boolean clry = (lay > rby);
                boolean crly = (ray > lby);
                boolean clrx = (lax > rbx);
                boolean crlx = (rax > lbx);

                if(!clry && crly && !clrx && crlx) {
                    return true;
                }

                return false;

            } else {
                boolean clly = (lay > lby);
                boolean crry = (ray > rby);
                boolean cllx = (lax > lbx);
                boolean crrx = (rax > rbx);

                if(clly == crry || cllx == crrx) {

                    return true;
                }

                return false;
            }
        } else {
            if(b.testInside) {
                boolean clly = (lay > lby);
                boolean crry = (ray > rby);
                boolean cllx = (lax > lbx);
                boolean crrx = (rax > rbx);

                if(clly == crry || cllx == crrx) {
                    return true;
                }

                return false;
            } else {
                return false;
            }
        }
    }

}
