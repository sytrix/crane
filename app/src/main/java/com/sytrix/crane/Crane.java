package com.sytrix.crane;

import android.graphics.Canvas;
import android.graphics.PointF;

import com.sytrix.crane.collision.CollisionBox;

public class Crane {


    private float rotFirstArm;
    private float rotSecondArm;
    private float lengthFirstArm;
    private float lengthSecondArm;
    private float lengthRope;
    private float constArmWidth;
    private float constRopeWidth;
    private float constMagnetWidth;
    private float constBaseWidth;
    RectangleShape rectBase;
    RectangleShape rectFirstArm;
    RectangleShape rectSecondArm;
    RectangleShape rectRope;
    RectangleShape rectMagnet;
    CollisionBox boxBase;
    CollisionBox boxMagnet;
    Crate crateGrab;
    private float w, h;

    public Crane() {

        rotFirstArm = 180.f;
        rotSecondArm = 180.f;

        lengthFirstArm = 400.f;
        lengthSecondArm = 300.f;
        lengthRope = 100.f;

        constArmWidth = 35.f;
        constMagnetWidth = 35.f;
        constRopeWidth = 8.f;
        constBaseWidth = 280.f;

        boxBase = new CollisionBox();
        boxMagnet = new CollisionBox();

        crateGrab = null;
    }

    public void screenUpdate(float w, float h) {

        this.w = w;
        this.h = h;

        rectBase = new RectangleShape();
        rectBase.setSize(constBaseWidth, constBaseWidth / 4.f);
        rectBase.setOrigin(constBaseWidth / 2.f, constBaseWidth / 4.f);
        rectBase.setPosition(w / 2.f, h);
        rectBase.setColor(128, 128, 128);

        rectFirstArm = new RectangleShape();
        rectFirstArm.setSize(constArmWidth, lengthFirstArm + constArmWidth);
        rectFirstArm.setOrigin(constArmWidth / 2.f, constArmWidth / 2.f);
        rectFirstArm.setPosition(w / 2.f, h - constArmWidth);
        rectFirstArm.setColor(80, 64, 49);

        rectSecondArm = new RectangleShape();
        rectSecondArm.setSize(constArmWidth, lengthSecondArm + constArmWidth);
        rectSecondArm.setOrigin(constArmWidth / 2.f, constArmWidth / 2.f);
        rectSecondArm.setColor(67, 50, 35);

        rectRope = new RectangleShape();
        rectRope.setSize(constRopeWidth, lengthRope);
        rectRope.setOrigin(constRopeWidth / 2.f, constRopeWidth / 2.f);
        rectRope.setColor(64, 64, 64);

        rectMagnet = new RectangleShape();
        rectMagnet.setSize(constMagnetWidth, constMagnetWidth / 2.5f);
        rectMagnet.setOrigin(constMagnetWidth / 2.f, constMagnetWidth / 5.f);
        rectMagnet.setColor(32, 32, 32);

        boxBase.setPosition(w / 2.f - constBaseWidth / 2.f, h - constBaseWidth / 4.f);
        boxBase.setSize(constBaseWidth, constBaseWidth / 4.f);
        boxBase.setGravity(false);

        boxMagnet.setSize(constMagnetWidth, constMagnetWidth / 2.5f);
        boxMagnet.setGravity(false);
    }

    public CollisionBox[] getCollisionBox() {
        return new CollisionBox[]{boxBase, boxMagnet};
    }

    public void rotateFirstArm(float value) {
        float oldValue = rotFirstArm;
        rotFirstArm += value;
        if(rotFirstArm > 225.f) {
            rotFirstArm = 225.f;
        } else if(rotFirstArm < 135.f) {
            rotFirstArm = 135.f;
        }

        if(this.testCollisionMagnet()) {
            rotFirstArm = oldValue;
        }
    }

    public void rotateSecondArm(float value) {
        float oldValue = rotSecondArm;
        rotSecondArm += value;
        if(rotSecondArm > 270.f) {
            rotSecondArm = 270.f;
        } else if(rotSecondArm < 90.f) {
            rotSecondArm = 90.f;
        }

        if(this.testCollisionMagnet()) {
            rotSecondArm = oldValue;
        }
    }

    public void resizeRope(float value) {
        float oldValue = lengthRope;
        lengthRope += value;
        if(lengthRope < 50.f) {
            lengthRope = 50.f;
        } else if(lengthRope > 450.f) {
            lengthRope = 450.f;
        }

        rectRope.setSize(constRopeWidth, lengthRope);

        if(this.testCollisionMagnet()) {
            lengthRope = oldValue;
        }
    }

    public void attachCrate(Crate crate) {
        if (this.crateGrab == null) {

            PointF oldPosition = crate.getPositionByMagnet();

            PointF point = this.getMagneticPoint();
            crate.setPositionByMagnet(point.x, point.y + 10.f);
            if (crate.getBox().move(0.f, 0.f)) {
                crate.getBox().setGravity(false);
                this.crateGrab = crate;
            } else {
                crate.setPositionByMagnet(oldPosition.x, oldPosition.y);

            }



        }
    }

    public void detachCrate() {
        if(this.crateGrab != null) {
            this.crateGrab.getBox().setGravity(true);
            this.crateGrab = null;
        }
    }

    public PointF getMagneticPoint() {
        float pfx = (float)-Math.sin(rotFirstArm * Math.PI / 180.f) * lengthFirstArm + w / 2.f;
        float pfy = (float)Math.cos(rotFirstArm * Math.PI / 180.f) * lengthFirstArm + h - constArmWidth;

        float psx = (float)-Math.sin(rotSecondArm * Math.PI / 180.f) * lengthSecondArm + pfx;
        float psy = (float)Math.cos(rotSecondArm * Math.PI / 180.f) * lengthSecondArm + pfy;

        float prx = psx;
        float pry = lengthRope + psy;

        return new PointF(prx, pry);
    }

    private boolean testCollisionMagnet() {
        PointF point = this.getMagneticPoint();

        boxMagnet.setPosition(point.x - 25.f, point.y - 20.f);

        if(this.crateGrab == null) {
            if (!boxMagnet.move(0.f, 0.f)) {
                return true;
            }
        } else {
            crateGrab.setPositionByMagnet(point.x, point.y + 10.f);

            if (!crateGrab.getBox().move(0.f, 0.f)) {
                return true;
            }
        }

        return false;
    }

    public void draw(Canvas canvas) {

        rectFirstArm.setRotation(rotFirstArm);
        rectSecondArm.setRotation(rotSecondArm);

        float pfx = (float)-Math.sin(rotFirstArm * Math.PI / 180.f) * lengthFirstArm + w / 2.f;
        float pfy = (float)Math.cos(rotFirstArm * Math.PI / 180.f) * lengthFirstArm + h - constArmWidth;
        rectSecondArm.setPosition(pfx, pfy);

        float psx = (float)-Math.sin(rotSecondArm * Math.PI / 180.f) * lengthSecondArm + pfx;
        float psy = (float)Math.cos(rotSecondArm * Math.PI / 180.f) * lengthSecondArm + pfy;
        rectRope.setPosition(psx, psy);

        float prx = psx;
        float pry = lengthRope + psy;
        rectMagnet.setPosition(prx, pry);

        rectFirstArm.draw(canvas);
        rectBase.draw(canvas);
        rectSecondArm.draw(canvas);
        rectRope.draw(canvas);
        rectMagnet.draw(canvas);
    }
}
