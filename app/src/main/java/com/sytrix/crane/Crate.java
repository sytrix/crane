package com.sytrix.crane;

import android.graphics.Canvas;
import android.graphics.PointF;

import com.sytrix.crane.collision.CollisionBox;

public class Crate {


    private RectangleShape rectBack;
    private RectangleShape rectPlank1;
    private RectangleShape rectPlank2;
    private RectangleShape rectMagnet;
    private CollisionBox boxBack;
    private float size;     // crate size
    private float x, y;     // position

    public Crate() {
        size = 120.f;
        x = 0.f;
        y = 0.f;

        rectBack = new RectangleShape();
        rectBack.setSize(size, size);
        rectBack.setColor(199, 133, 66);

        rectPlank1 = new RectangleShape();
        rectPlank1.setSize(size, size / 6.f);
        rectPlank1.setColor(166, 99, 44);
        rectPlank1.setOrigin(size / 2.f, size / 12.f);
        rectPlank1.setRotation(45.f);

        rectPlank2 = new RectangleShape();
        rectPlank2.setSize(size, size / 6.f);
        rectPlank2.setColor(166, 99, 44);
        rectPlank2.setOrigin(size / 2.f, size / 12.f);
        rectPlank2.setRotation(-45.f);

        rectMagnet = new RectangleShape();
        rectMagnet.setSize(size / 4.f, size / 16.f);
        rectMagnet.setOrigin(size / 8.f, size / 16.f);
        rectMagnet.setColor(32, 32, 32);

        boxBack = new CollisionBox();
        boxBack.setSize(size, size);
        boxBack.setGravity(true);
    }

    public CollisionBox getBox() {
        return this.boxBack;
    }

    public void setPositionByMagnet(float x, float y) {
        this.setPosition(x - size / 2.f, y);
    }

    public PointF getPositionByMagnet() {
        return new PointF(x + size / 2.f, y);
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
        rectBack.setPosition(x, y);
        rectPlank1.setPosition(x + size / 2.f, y + size / 2.f);
        rectPlank2.setPosition(x + size / 2.f, y + size / 2.f);
        rectMagnet.setPosition(x + size / 2.f, y);
        boxBack.setPosition(x, y);
    }

    public PointF getMagneticPoint() {
        return new PointF(x + size / 2, y);
    }


    public void draw(Canvas canvas) {

        PointF point = boxBack.getPosition();
        this.setPosition(point.x, point.y);

        rectBack.draw(canvas);
        rectPlank1.draw(canvas);
        rectPlank2.draw(canvas);
        rectMagnet.draw(canvas);

    }


}
