package com.sytrix.crane;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;

import com.sytrix.crane.collision.CollisionBox;
import com.sytrix.crane.collision.CollisionManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyCanvas extends View {

    private float w, h;
    private boolean firstRender;
    Crane crane;
    List<Crate> crates;
    CircleShape circleMagnetEffect;
    SeekBar barFirstArm;
    SeekBar barSecondArm;
    SeekBar barRope;
    PushButton buttonMagnet;
    CollisionManager collisionManager;
    CollisionBox windowBox;

    public MyCanvas(Context context) {
        super(context);

        firstRender = true;

        crane = new Crane();
        crates = new ArrayList<>();

        circleMagnetEffect = new CircleShape();
        circleMagnetEffect.setColor(0, 128, 0, 128);
        circleMagnetEffect.setRadius(50.f);

        barFirstArm = new SeekBar();
        barSecondArm = new SeekBar();
        barRope = new SeekBar();

        buttonMagnet = new PushButton();


        collisionManager = new CollisionManager();
        CollisionBox[] boxesCrane = crane.getCollisionBox();
        collisionManager.addBoxes(boxesCrane);

        windowBox = new CollisionBox();
        windowBox.setGravity(false);
        windowBox.setTestInside(false);

        collisionManager.addBox(windowBox);

    }

    public void screenUpdate() {
        float marginBarX = 20.f;
        float marginBarY = 80.f;
        float barWidth = 500.f;
        float barHeight = 30.f;

        crane.screenUpdate(w, h);

        for(int i = 0; i < 5; i++) {
            Crate c = new Crate();
            c.setPosition(w / 2.f - 290.f - 140.f * i, h - 200.f);
            crates.add(c);

            collisionManager.addBox(c.getBox());
        }

        barFirstArm.setPosition(marginBarX, h - marginBarY);
        barFirstArm.setSize(barWidth, barHeight);

        barSecondArm.setPosition(w - marginBarX - barWidth, h - marginBarY);
        barSecondArm.setSize(barWidth, barHeight);

        barRope.setPosition(50.f, 20.f);
        barRope.setSize(30.f, 500.f);

        float buttonSize = 150.f;

        buttonMagnet.setPosition(w - buttonSize - 20.f, 20.f);
        buttonMagnet.setSize(buttonSize, buttonSize);

        windowBox.setSize(w, h);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        barFirstArm.onTouchEvent(ev);
        barSecondArm.onTouchEvent(ev);
        barRope.onTouchEvent(ev);
        buttonMagnet.onTouchEvent(ev);

        return true;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(firstRender) {
            w = canvas.getWidth();
            h = canvas.getHeight();
            this.screenUpdate();
            firstRender = false;
        }

        collisionManager.update();

        canvas.drawColor(0xFF374080);

        crane.rotateFirstArm(barFirstArm.getValue());
        crane.rotateSecondArm(barSecondArm.getValue() * 1.5f);
        crane.resizeRope(barRope.getValue() * 3.5f);

        PointF ptCrane = crane.getMagneticPoint();

        Iterator<Crate> iterator = crates.iterator();
        while(iterator.hasNext()) {
            Crate c = iterator.next();
            PointF ptObj = c.getMagneticPoint();

            float cx = ptCrane.x - ptObj.x;
            float cy = ptCrane.y - ptObj.y;
            float d = (float) Math.sqrt(cx * cx + cy * cy);

            c.draw(canvas);

            if (d < 40.f) {
                if (buttonMagnet.isPushed()) {
                    crane.attachCrate(c);
                } else {
                    crane.detachCrate();
                    circleMagnetEffect.setPosition(ptCrane.x, ptCrane.y);
                    circleMagnetEffect.draw(canvas);
                }
            }
        }

        crane.draw(canvas);

        barFirstArm.draw(canvas);
        barSecondArm.draw(canvas);
        barRope.draw(canvas);

        buttonMagnet.draw(canvas);

        this.invalidate();
    }
}
