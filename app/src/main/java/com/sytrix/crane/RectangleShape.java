package com.sytrix.crane;

import android.graphics.Canvas;
import android.graphics.Paint;

public class RectangleShape {

    private float x, y;
    private float h, w;
    private float ox, oy;
    private float rotation;
    private float roundx, roundy;
    private Paint paintFill;
    private Paint paintOutline;

    public RectangleShape() {
        x = 0.f;
        y = 0.f;
        w = 0.f;
        h = 0.f;
        ox = 0.f;
        oy = 0.f;
        roundx = 0.f;
        roundy = 0.f;
        rotation = 0.f;

        paintFill = new Paint();
        paintFill.setStyle(Paint.Style.FILL);
        paintOutline = new Paint();
        paintOutline.setStyle(Paint.Style.STROKE);
        paintOutline.setStrokeWidth(2);
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void move(float mx, float my) {
        this.x += mx;
        this.y += my;

    }

    public void setSize(float w, float h) {
        this.w = w;
        this.h = h;
    }

    public void setOrigin(float ox, float oy) {
        this.ox = ox;
        this.oy = oy;
    }

    public void setColor(int r, int g, int b, int a) {
        int color = (a << 24) + (r << 16) + (g << 8) + b;
        this.paintFill.setColor(color);
    }

    public void setColor(int r, int g, int b) {
        this.setColor(r, g, b, 0xFF);
    }

    public void setOutlineColor(int r, int g, int b, int a) {
        int color = (a << 24) + (r << 16) + (g << 8) + b;
        paintOutline.setColor(color);
    }

    public void setOutlineColor(int r, int g, int b) {
        this.setOutlineColor(r, g, b, 0xFF);
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public void setRound(float roundx, float roundy) {
        this.roundx = roundx;
        this.roundy = roundy;
    }

    public void setRound(float round) {
        this.setRound(round, round);
    }

    public void draw(Canvas canvas) {

        canvas.save();
        canvas.translate(x, y);
        canvas.rotate(rotation);
        canvas.drawRoundRect(-ox, -oy, w-ox, h-oy, roundx, roundy, paintFill);
        canvas.drawRoundRect(-ox, -oy, w-ox, h-oy, roundx, roundy, paintOutline);
        canvas.restore();
    }
}

