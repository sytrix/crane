package com.sytrix.crane;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;

public class PushButton {

    private float x, y;
    private float w, h;
    private RectangleShape rectButton;
    private boolean isPushed;
    private String title;
    private Paint paint;

    public PushButton() {
        rectButton = new RectangleShape();
        rectButton.setOutlineColor(255, 255, 255, 255);
        rectButton.setRound(15.f);
        this.setPushed(false);

        title = "Magn";
        paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(Color.BLACK);


    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;

        rectButton.setPosition(x, y);
    }

    public void setSize(float w, float h) {
        this.w = w;
        this.h = h;

        paint.setTextSize(50.f);
        rectButton.setSize(w, h);
    }

    public boolean isPushed() {
        return isPushed;
    }

    public void setPushed(boolean isPushed) {
        this.isPushed = isPushed;

        if(isPushed) {
            rectButton.setColor(128, 128, 128);
        } else {
            rectButton.setColor(255, 255, 255);
        }
    }

    public void onTouchEvent(MotionEvent ev) {

        final int actionMasked = ev.getActionMasked();
        float touchTolerance = 30.f;

        switch (actionMasked) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {

                float x = ev.getX();
                float y = ev.getY();

                if((this.x - touchTolerance) <= x && (this.x + w + touchTolerance) > x && (this.y - touchTolerance) <= y && (this.y + h + touchTolerance) > y) {
                    this.setPushed(!isPushed);
                }

                break;
            }


        }
    }

    public void draw(Canvas canvas) {

        rectButton.draw(canvas);

        canvas.drawText(title, x + w / 2.f, y + h / 2 + 18.f, paint);
    }



}
