package com.sytrix.crane;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import static android.content.ContentValues.TAG;

public class SeekBar {


    private RectangleShape rectBar;
    private RectangleShape rectCursor;
    private float x, y;
    private float w, h;
    private float outMargin;
    private float value;
    private int currentActionPointerIndex;
    private boolean isHorizontal;

    public SeekBar() {


        rectBar = new RectangleShape();
        rectBar.setColor(255, 255, 255, 64);
        rectBar.setOutlineColor(255, 255, 255, 255);
        rectCursor = new RectangleShape();
        rectCursor.setColor(255, 255, 255, 255);
        rectCursor.setOutlineColor(255, 255, 255, 255);

        x = 0.f;
        y = 0.f;
        w = 0.f;
        h = 0.f;
        value = 0.f;
        outMargin = 0.f;

        currentActionPointerIndex = -1;
        isHorizontal = true;
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
        this.updateRect();
    }

    public void setSize(float w, float h) {
        this.w = w;
        this.h = h;

        isHorizontal = (w > h);

        this.updateRect();
    }

    public float getValue() {
        return this.value;
    }

    private void updateRect() {
        if(isHorizontal) {
            outMargin = h;
        } else {
            outMargin = w;
        }

        rectBar.setPosition(x, y);
        rectBar.setSize(w, h);
        rectBar.setRound(10);

        if(isHorizontal) {
            rectCursor.setOrigin(h / 2.f + outMargin, outMargin);
            rectCursor.setSize(h + outMargin * 2.f, h + outMargin * 2.f);
        } else {
            rectCursor.setOrigin(outMargin, w / 2.f + outMargin);
            rectCursor.setSize(w + outMargin * 2.f, w + outMargin * 2.f);

        }
        rectCursor.setRound(outMargin / 2.f);

        this.updateCursorPos();
    }

    private void updateCursorPos() {
        if(isHorizontal) {
            rectCursor.setPosition(x + w / 2.f + this.value * w / 2.f, y);
        } else {
            rectCursor.setPosition(x, y + h / 2.f + this.value * h / 2.f);
        }
    }

    public void onTouchEvent(MotionEvent ev) {

        final int actionMasked = ev.getActionMasked();
        final int actionIndex = ev.getActionIndex();

        switch (actionMasked) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {

                if(currentActionPointerIndex == -1) {
                    currentActionPointerIndex = actionIndex;
                    float x = ev.getX(currentActionPointerIndex);
                    float y = ev.getY(currentActionPointerIndex);

                    if(isHorizontal) {
                        if (this.x <= x && (this.x + w) > x && (this.y - outMargin) <= y && (this.y + outMargin + h) > y) {
                            this.value = ((x - this.x) / w * 2.f) - 1.f;
                            this.updateCursorPos();
                        } else {
                            currentActionPointerIndex = -1;
                        }
                    } else {
                        if ((this.x - outMargin) <= x && (this.x + outMargin + w) > x && this.y <= y && (this.y + h) > y) {
                            this.value = ((y - this.y) / h * 2.f) - 1.f;
                            this.updateCursorPos();
                        } else {
                            currentActionPointerIndex = -1;
                        }
                    }
                } else if(currentActionPointerIndex >= actionIndex) {
                    //Log.d(TAG, "COLLISION");
                    currentActionPointerIndex++;
                }
                //Log.d(TAG, "[D] pointer:" + actionMasked + " index:" + actionIndex);

                break;
            }

            case MotionEvent.ACTION_MOVE: {

                if(currentActionPointerIndex != -1) {
                    if(isHorizontal) {
                        float x = ev.getX(currentActionPointerIndex);

                        this.value = ((x - this.x) / w * 2.f) - 1.f;
                        if (this.value < -1.f) {
                            this.value = -1.f;
                        }
                        if (this.value > 1.f) {
                            this.value = 1.f;
                        }
                    } else {
                        float y = ev.getY(currentActionPointerIndex);

                        this.value = ((y - this.y) / h * 2.f) - 1.f;
                        if (this.value < -1.f) {
                            this.value = -1.f;
                        }
                        if (this.value > 1.f) {
                            this.value = 1.f;
                        }
                    }
                    this.updateCursorPos();
                }

                break;
            }

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP: {

                //Log.d(TAG, "[U] pointer:" + actionMasked + " index:" + actionIndex);

                if(currentActionPointerIndex == actionIndex) {
                    this.value = 0.f;
                    this.updateCursorPos();
                    currentActionPointerIndex = -1;
                } else if(actionIndex < currentActionPointerIndex) {
                    currentActionPointerIndex--;
                }


                break;
            }


        }
    }

    public void draw(Canvas canvas) {
        rectBar.draw(canvas);
        rectCursor.draw(canvas);
    }
}
