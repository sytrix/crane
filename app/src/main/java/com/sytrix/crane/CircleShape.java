package com.sytrix.crane;

import android.graphics.Canvas;
import android.graphics.Paint;

public class CircleShape {

    private float x, y;
    private float radius;
    private Paint paint;

    public CircleShape() {
        x = 0.f;
        y = 0.f;
        radius = 0.f;

        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void move(float mx, float my) {
        this.x += mx;
        this.y += my;

    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public void setColor(int r, int g, int b, int a) {
        int color = (a << 24) + (r << 16) + (g << 8) + b;
        this.paint.setColor(color);
    }

    public void setColor(int r, int g, int b) {
        this.setColor(r, g, b, 0xFF);
    }

    public void draw(Canvas canvas) {
        canvas.drawCircle(x, y, radius, paint);
    }
}
